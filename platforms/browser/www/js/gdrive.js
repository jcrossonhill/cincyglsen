var gdrive;

gdrive.uploadFileContents = function(contents) {
	var request = gapi.client.request({
        'path': '/upload/drive/v2/files',
        'method': 'POST',
        'params': {'uploadType': 'multipart'},
        'headers': {
          'Content-Type': 'multipart/mixed; '
        },
        'body': contents});

	var promise = new Promise(function(resolve, reject) {
		request.execute(function() {
			resolve();
		});
	};
	
	return promise;
};
