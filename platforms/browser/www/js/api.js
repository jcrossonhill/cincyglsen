var api;
var network;
var storage;

api.addOnConnectedHandler = function (func) {
	network.addOnConnectedHandler(func);
};

api.addPersonRecord = function(person) {
	storage.addPersonRecord(person);
};

api.writeToRemote = function() {
	return storage.writeToRemote();
};

api.isSaving() = function() {
	return storage.isWriting;
}
