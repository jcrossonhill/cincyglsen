/**
 * Created by manishmishra on 7/14/15.
 */
var glsenApp = angular.module('glsenApp',['ngRoute','ngResource','ui.bootstrap']);

glsenApp.config(function($routeProvider){
    'use strict'

    $routeProvider
        .when('/',{
                templateUrl:'./views/sign_up.html' ,
                controller:'signUpController'
        })
    .otherwise('/',{
    		templateUrl:'./views/sign_up.html' ,
    		controller:'signUpController'
        });

});