/**
 * New node file
 */

glsenApp.service('apiService',['gdriveService','networkService','storageService',function(gdrive, network,storage){
	'use strict';
	
	var api =this;

	api.addOnConnectedHandler = function (func) {
		network.addOnConnectedHandler(func);
	};

	api.addPersonRecord = function(person) {
		console.log("Person"+ person);
		storage.addPersonRecord(person);
	};

	/*api.writeToRemote = function() {
		return storage.writeToRemote();
	};*/
	
	api.writeToRemoteIfConnected = function() {
		return storage.writeToRemoteIfConnected();
	};
	
	api.isSaving= function() {
		return storage.isWriting;
	}
}]);