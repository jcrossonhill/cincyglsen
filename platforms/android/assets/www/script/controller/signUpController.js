/**
 * Created by swogc on 10/21/2016.
 */

glsenApp.controller('signUpController',['$rootScope','$scope','$log','$http','$location','apiService',
                                function($rootScope,$scope,$log,$http,$location,api){
    'use strict';

    
    var _init = function(){
    	$scope.user = {};
    }
    
    //Save to local
    $scope.save = function(){
    	
    	console.log($scope.user);
    	
    	//Convert to cvs.
    	// save to local
    	api.addPersonRecord(person);
    	
    	$scope.user = {};
    };

    $scope.addPersonRecord = function(person) {
        return api.addPersonRecord(person);
    };

    $scope.writeToRemote = function() {
        return api.writeToRemote();
    };

    $scope.isSaving = function() {
        return api.isSaving();
    };

    $scope.isConnected = function(){
        return api.isConnected();
    };
    
    //call _init
    _init();

}])