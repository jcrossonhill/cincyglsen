/**
 * New node file
 */

glsenApp.service('storageService',['gdriveService','networkService',function(gdrive,network){
	'use strict';
	
	var storage =this;




	storage.existingRecordsKey = "existingRecords";
	storage.existingColumnsKey = "existingColumns";
	storage.writing = false;

	storage.writeToRemoteIfConnected = function() {
		var promise = new Promise(function(resolve, reject) {
			if(network.isConnected() === false) {
				resolve();

				return;
			}

			storage.writeToRemote()
				.then(function() {
					resolve();
				});
		});

		return promise;
	};

	storage.addPersonRecord = function(person){
		var promise = new Promise(function(resolve, reject) {					
			while(storage.writing === true) {
			}
			
			storage.writing = true;

			storage.commit(person);

			storage.writing = false;
			
			storage.writeToRemoteIfConnected()
				.then(function() {
					resolve();
				});
		});

		return promise;
	};

	storage.writeToRemote = function() {
		var promise = new Promise(function(resove, reject) {
			while(storage.writing === true) {			
			}
			
			storage.writing = true;

			var existingColumns = storage.getExistingColumns();
			var existingRecords = storage.getExistingRecords();

			if(existingRecords === "") {
				resolve();

				return;
			}
				
			var toWrite = existingColumns + "\n" + existingRecords;
			
			gdrive.uploadFileContents(toWrite)
				.then(function() {
					internalStorage.setItem(storage.existingRecordsKey, "");

					storage.writing = false;

					resolve();
				});
		});
		
		return promise;
	};

	network.addOnConnectedHandler(storage.writeToRemoteIfConnected);
	setTimeout(storage.writeToRemoteIfConnected, 5000);

	storage.getExistingRecords = function() {
		var existingRecords;
		
		try {
			existingRecords = internalStorage.getItem(storage.existingRecordsKey);
		}
		catch (ex) {
			existingRecords = "";
		}
		
		return existingRecords;
	};

	storage.getExistingColumns = function() {
		var existingColumns;
		
		try {
			existingColumns = internalStorage.getItem(storage.existingColumnsKey);
		}
		catch (ex) {
			existingColumns = "";
		}
		
		return existingColumns;
	};

	storage.commit = function(person) {
		var internalStorage = window.localStorage;
		
		var existingRecords = storage.getExistingRecords();	
		
		var keyValuePairsOfPropertiesAndValues = storage.convertPersonObjectToArrayOfKeyValuePairsOfPropertiesAndValues(person);
		
		var valuesAsString = storage.convertArrayOfKeyValuePairsOfPropertiesAndValuesToValuesString(keyValuePairsOfPropertiesAndValues);
		var columnsAsString = storage.convertArrayOfKeyValuePairsOfPropertiesAndValuesToColumnsString(keyValuePairsOfPropertiesAndValues);
		
		existingRecords += "\n" + valuesAsString;
		
		internalStorage.setItem(storage.existingRecordsKey, existingRecords);
		internalStorage.setItem(storage.existingColumnsKey, columnsAsString);
	};

	storage.convertPersonObjectToArrayOfKeyValuePairsOfPropertiesAndValues = function(person) {
		var arr = [];
		
		for(var key in person){
			if(person.hasOwnProperty(key)) {
				var pair = {
					propertyName : key,
					valueOf : person[key]
				};
				
				arr.push();
			}
		}
		
		return arr;
	};

	storage.convertArrayOfKeyValuePairsOfPropertiesAndValuesToColumnsString = function(person) {
		var output = "";
		
		for(var pair in person){
			output += pair.propertyName + ";";
		}
		
		return output;
	};

	storage.convertArrayOfKeyValuePairsOfPropertiesAndValuesToValuesString = function(person) {
		var output = "";
		
		for(var pair in person){
			output += pair.valueOf + ";";
		}
		
		return output;
	};


}]);