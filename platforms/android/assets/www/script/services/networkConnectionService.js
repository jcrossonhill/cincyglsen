/**
 * New node file
 */

glsenApp.service('networkService',function(){
	'use strict';
	
	var network =this;

	network.isConnected = function () {
	    var networkState = navigator.connection.type;

		return networkState === Connection.CELL_3G || 
			networkState === Connection.CELL_4G || 
			networkState === Connection.WIFI || 
			networkState === Connection.ETHERNET;
	 };
	 
	 network.addOnConnectedHandler = function(func) {
		document.addEventListener("online", func, false);
	 };


});