/**
 * Created by SWOGC on 10/21/16.
 * This contains routes information of the application  
 * default is views/sign_up.html 
 */
var glsenApp = angular.module('glsenApp',['ngRoute','ngResource','ui.bootstrap']);

glsenApp.config(function($routeProvider){
    'use strict'

    $routeProvider
        .when('/',{
                templateUrl:'./views/sign_up.html' ,
                controller:'signUpController'
        })
    .otherwise('/',{
    		templateUrl:'./views/sign_up.html' ,
    		controller:'signUpController'
        });

});