/**
 * Created by swogc on 10/21/2016.
 */

glsenApp.controller('signUpController',['$rootScope','$scope','$q','$timeout','$log','$http','$location','apiService',
                                function($rootScope,$scope,$q,$timeout,$log,$http,$location,api){
  'use strict';
    
  var _init = function(){
    // Scroll to top of window
    $('html,body').animate({ scrollTop: 0 }, 'slow');

    $scope.user = {};
    if($scope.userForm){
      $scope.userForm.$setPristine();
    }

    $timeout(function(){
      $scope.saveStatus = null;
    }, 3000);
  };
    
  //Save to local
  $scope.save = function(){    
    // Scroll to top of window
    $('html,body').animate({ scrollTop: 0 }, 'slow');

    if($scope.userForm.$valid){				
      console.log($scope.user);    	

      $timeout(function(){
        $scope.saveStatus = 'saving';
      }, 0);

      var addPersonRecordPromise = addPersonRecord($scope.user);
      addPersonRecordPromise
        .then(function resolve(result){
          $scope.saveStatus = result;
          // Reset form
          $scope.resetForm();
        }, function reject(error){
          $scope.saveStatus = error;
        });
    }
  };

  $scope.resetForm = function(){	
		_init();
	};

  $scope.sync = function(){
    // Scroll to top of window
    $('html,body').animate({ scrollTop: 0 }, 'slow');

    $timeout(function(){
      $scope.syncStatus = 'syncing';
    }, 0);

    var syncSuccess = function(){
      $scope.syncStatus = 'success';
    };

    var syncFailed = function(){
      $scope.syncStatus = 'failed';
    };

    var syncClear = function(){
      $scope.syncStatus = null;
    };

    api.writeToRemoteIfConnected()
      .then(function resolve(result){
        $timeout(syncSuccess, 3000)
          .then(function () { return $timeout(syncClear, 2000); });
      }, function reject(error){
        $timeout(syncFailed, 3000)
          .then(function () { return $timeout(syncClear, 2000); });
      });
  };
  
  var addPersonRecord = function(user){
    var deferred = $q.defer();

    $timeout(function(){
      try{
        api.addPersonRecord(user);
        deferred.resolve('success');
      }
      catch(e){
        deferred.reject('failed');
      }
    }, 2000);

    return deferred.promise
  };

  //call _init
  _init();

}]);