/**
 * New node file
 */

glsenApp.service('storageService',['gdriveService','networkService',function(gdrive,network){
	'use strict';
	
	var storage =this;

	var internalStorage = window.localStorage;
	storage.existingRecordsKey = "existingRecords";
	storage.existingColumnsKey = "existingColumns";
	storage.writing = false;

	storage.writeToRemoteIfConnected = function() {
		var promise = new Promise(function(resolve, reject) {
			if(network.isConnected() === false) {
				reject("Not connected to the network.");
			}
			storage.writeToRemote()
				.then(function() {
					resolve();
				}, function(error) {
				    reject(error)
				});
		});

		return promise;
	};

	storage.addPersonRecord = function(person){
		storage.commit(person);
		//return promise;
	};

	storage.writeToRemote = function() {
		var promise = new Promise(function(resolve, reject) {
			var existingColumns = storage.getExistingColumns();
			var existingRecords = storage.getExistingRecords();

			if(existingRecords === "") {
				resolve();
				return;
			}

			var toWrite = existingColumns + "\n" + existingRecords;
			gdrive.uploadFileContents(toWrite, true)
				.then(function() {
					internalStorage.setItem(storage.existingRecordsKey, "");
					resolve();
				}, function(error) {
				    reject(error);
				});
		});

		return promise;
	};

	storage.getExistingRecords = function() {
		var existingRecords;

		try {
			existingRecords = internalStorage.getItem(storage.existingRecordsKey);
		}
		catch (ex) {
			existingRecords = "";
		}

		return existingRecords;
	};

	storage.getExistingColumns = function() {
		var existingColumns;

		try {
			existingColumns = internalStorage.getItem(storage.existingColumnsKey);
		}
		catch (ex) {
			existingColumns = "";
		}

		return existingColumns;
	};

	storage.commit = function(person) {

		var existingRecords = storage.getExistingRecords();

		var keyValuePairsOfPropertiesAndValues = storage.convertPersonObjectToArrayOfKeyValuePairsOfPropertiesAndValues(person);

		var valuesAsString = storage.convertArrayOfKeyValuePairsOfPropertiesAndValuesToValuesString(keyValuePairsOfPropertiesAndValues);
		var columnsAsString = storage.convertArrayOfKeyValuePairsOfPropertiesAndValuesToColumnsString(keyValuePairsOfPropertiesAndValues);

		existingRecords += "\n" + valuesAsString;

		internalStorage.setItem(storage.existingRecordsKey, existingRecords);
		internalStorage.setItem(storage.existingColumnsKey, columnsAsString);
	};

	storage.convertPersonObjectToArrayOfKeyValuePairsOfPropertiesAndValues = function(person) {
		var arr = [];

		for(var key in person){
			if(person.hasOwnProperty(key)) {
				var pair = {
					propertyName : key,
					valueOf : person[key]
				};

				arr.push(pair);
			}
		}

		return arr;
	};

	storage.convertArrayOfKeyValuePairsOfPropertiesAndValuesToColumnsString = function(personArray) {
		var output = "";
		
		angular.forEach(personArray ,function(person){
            output += "\"" + person.propertyName.replace(/"/, '\\"') + "\",";
        });

		return output.substring(0, output.length - 1);
	};

	storage.convertArrayOfKeyValuePairsOfPropertiesAndValuesToValuesString = function(personArray) {
		var output = "";

		angular.forEach(personArray ,function(person){
			if(typeof person.valueOf === 'boolean')
				output += "\"" + person.valueOf + "\",";
			else
				output += "\"" + person.valueOf.replace(/"/, '\\"') + "\",";
        });

		return output.substring(0, output.length - 1);
	};
}]);