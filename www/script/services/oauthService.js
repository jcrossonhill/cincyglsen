/**
 * New node file
 */

glsenApp.service('oauthService',['$http', 'configurationService', function($http, cfg) {
	'use strict';

	var oauth = this;
	oauth.getAuthCode = function() {
	    var promise = new Promise(function(resolve, reject) {
    		var reference = cordova.InAppBrowser.open(cfg.authUrl, "_self", "location=no,hidden=yes");
            var closeListener = function(inAppBrowserEvent) {
                resolve();
            };
    		reference.addEventListener("loadstart", function(inAppBrowserEvent) {
        		var url = inAppBrowserEvent.url;
        		if(!url.startsWith(cfg.redirectUri)) {
    				return;
    			}
    			reference.removeEventListener("exit", closeListener);
    			var codeMatch = /code=(.+)$/.exec(url);
    			if (codeMatch !== null) {
    			    reference.close();
    				resolve(codeMatch[1]);
    			} else {
    			    reject("Cannot obtain OAuth2 code.");
    			}
    		});
    		reference.addEventListener("exit", closeListener);
    		reference.show();
    	});

        return promise;
	};

    oauth.getTokensFromCode = function(code) {
        var promise = new Promise(function(resolve, reject) {
            $http.post(cfg.tokenUrl,
                $.param({
            	    code: code,
            		client_id: cfg.clientId,
                    client_secret: cfg.clientSecret,
                    redirect_uri: cfg.redirectUri,
                    grant_type: "authorization_code"
                }),
                {
                    headers:{
                        'Content-Type':'application/x-www-form-urlencoded;charset=utf-8'
                    }
                })
            .then(function(successParameter) {
                var tokens = {
                    refreshToken: successParameter.data.refresh_token,
                    accessToken: successParameter.data.access_token,
                    tokenType: successParameter.data.token_type
                };
                resolve(tokens);
            }, function(failureParameter, abc, xyz) {
                reject("Cannot exchange code for tokens.");
            });
        });
        return promise;
    };

    oauth.refreshAuthToken = function(refreshToken) {
        var promise = new Promise(function(resolve, reject) {
            $http.post(cfg.tokenUrl,
                $.param({
                    client_id: cfg.clientId,
                    client_secret: cfg.clientSecret,
                    refresh_token: refreshToken,
                    grant_type: "refresh_token"
                }),
                {
                    headers:{
                        'Content-Type':'application/x-www-form-urlencoded;charset=utf-8'
                    }
                })
            .then(function(successParameter) {
                var tokens = {
                    accessToken: successParameter.data.access_token,
                    tokenType: successParameter.data.token_type
                };
                resolve(tokens);
            }, function(failureParameter, abc, xyz) {
                reject("Cannot exchange refresh token for auth token.");
            });
        });
        return promise;
    };
}]);