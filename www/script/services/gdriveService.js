/**
 * New node file
 */


glsenApp.service('gdriveService', ['$http', 'oauthService', function($http, oauthService){
	'use strict';

	var gdrive =this;
    var accessToken = null;
    var accessTokenType = null;
    var internalStorage = window.localStorage;
    var tokenKey = "longTermToken";

	gdrive.uploadFileContents = function(contents, retryOnAuth) {
		var promise = new Promise(function(resolve, reject) {
		    var url = 'https://www.googleapis.com/drive/v3/files?access_token=' + accessToken;
		    var currentDate = new Date();
            var body = {
                name: "GLSEN_Registration_Upload-"
                    + currentDate.getFullYear()
                    + (currentDate.getMonth() + 1)
                    + currentDate.getDate()
                    + (currentDate.getHours() + 1)
                    + currentDate.getMinutes()
                    + currentDate.getSeconds()
                    + ".csv",
                mimeType: "text/csv"
            };
		    $http.post(url, body).then(function (data) {
		        var uploadUrl = 'https://www.googleapis.com/upload/drive/v3/files/' + data.data.id + '?uploadType=media&access_token=' + accessToken;
		        var headers = {
		            'Content-Type': 'text/csv'
		        };
                $http.patch(uploadUrl, contents).then(function (uploadData) {
                    resolve();
                }, function (error) {
                    reject(error);
                });
		    }, function (error) {
		        if (retryOnAuth && error.status == 401) {
		           gdrive.renewAuthorization().then(function () {
		                gdrive.uploadFileContents(contents, false);
						
						resolve();
		           }, function (error) {
		                reject(error);
		           })
		        } else {
		            reject("Not authorized to create in Google Drive.");
		        }
		    });
		});
		return promise;
	};

    gdrive.renewAuthorization = function() {
        var promise = new Promise(function(resolve, reject) {
            /*var refreshToken = internalStorage.getItem(tokenKey);
            if (refreshToken !== null && refreshToken !== "undefined") {
                oauthService.refreshAuthToken(refreshToken).then(function (tokens) {
                    accessToken = tokens.accessToken;
                    accessTokenType = tokens.tokenType;
                    resolve();
                }, function (error) {
                    oauthService.getAuthCode().then(function (code) {
                        oauthService.getTokensFromCode(code).then(function(tokens) {
                            internalStorage.setItem(tokenKey, tokens.refresh_token);
                            accessToken = tokens.accessToken;
                            accessTokenType = tokens.tokenType;
                            resolve();
                        }, function (error) {
                            reject(error);
                        });
                    }, function (error) {
                        reject(error);
                    });
                });
            } else {*/
                oauthService.getAuthCode().then(function (code) {
                    oauthService.getTokensFromCode(code).then(function(tokens) {
                        //internalStorage.setItem(tokenKey, tokens.refresh_token);
                        accessToken = tokens.accessToken;
                        accessTokenType = tokens.tokenType;
                        resolve();
                    }, function (error) {
                        reject(error);
                    });
                }, function (error) {
                    reject(error);
                });
            //}
        });
        return promise;
    };

	gdrive.getAuthorizationToken = function() {

		
		var promise = new Promise(function(resolve, reject) {
			var reference = cordova.InAppBrowser.open("https://accounts.google.com/o/oauth2/v2/auth?access_type=offline&scope=https://www.googleapis.com/auth/drive.file&redirect_uri=" + redirectUri + "&response_type=code&client_id=" + clientId, "_self", "location=no,hidden=yes");
			
			reference.addEventListener("loadstart", function(inAppBrowserEvent) {
				var url = inAppBrowserEvent.url;
				
				if(!url.includes("org.glsen.registrationapp")) {
					return;
				}
				
				var codeMatch = /code=(.+)$/.exec(url);
				
				if(codeMatch !== null) {
					var code = codeMatch[1];
					$http.post('https://www.googleapis.com/oauth2/v4/token',
						 $.param({
							code: code,
							client_id: clientId,
							client_secret: clientSecret,
							redirect_uri: redirectUri,
							grant_type: "authorization_code"
						}),
						{
							headers:{
								'Content-Type':'application/x-www-form-urlencoded;charset=utf-8'
							}
						})
					.then(function(successParameter) {
						internalStorage.setItem("refreshToken", successParameter.data.refresh_token);

						resolve(successParameter.data);
						
						reference.close();
					}, function(failureParameter, abc, xyz) {
						reference.close();
						
						reject("Unable to authenticate");
					});
					
					return;
				}
				
				reject("Unable to authenticate");
			});
			reference.show();
		});
		
		return promise;
	};		
}]);